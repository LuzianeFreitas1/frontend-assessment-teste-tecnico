# Teste técnico Luziane Freitas

### Acessando o repositório do projeto

<a href="https://gitlab.com/LuzianeFreitas1/le-tip">LeTip</a>

### Clonando o repositório

```
git clone https://gitlab.com/LuzianeFreitas1/le-tip.git
```
### Acessando a pasta do projeto

```
cd le-tip
```

### Instalando dependencias do projeto

```
yarn install
```

### Compilando projeto

```
yarn serve

http://localhost:8080/

* A porta onde o projeto esta rodando pode ser diferente cheque o terminal para acessar a correta.
```
